package com.example.demo.publish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.message.Message;

@RestController
public class Publisher {
	@Autowired
	private RedisTemplate<String, Object> redistemplate;

	@Autowired
	private ChannelTopic channelTopic;

	@GetMapping("/sendMessage/{message}")
	public String sendmessage(@PathVariable("message") String message) {
		Message messageObj = new Message(1, message);
		redistemplate.convertAndSend(channelTopic.getTopic(), messageObj.toString());
		return "message send";
	}
}
