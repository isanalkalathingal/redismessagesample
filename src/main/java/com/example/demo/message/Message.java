package com.example.demo.message;

public class Message {
	private final int id;
	private final String content;

	public Message(int id, String content) {
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	@Override
	  public String toString() {
	    return String.format("Message[id=%d, content='%s']", id, content);
	}
}
