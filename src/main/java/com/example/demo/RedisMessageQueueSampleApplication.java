package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisMessageQueueSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisMessageQueueSampleApplication.class, args);
	}

}
